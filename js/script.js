window.addEventListener("DOMContentLoaded", function() {

    this.window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "background": "#FFD700"
            },
            "button": {
                "background": "#000000"
            }
        },
        "position": "bottom-right",
        "content": {
            "href": "/p/cookies-policy.html"
        }
    })

})